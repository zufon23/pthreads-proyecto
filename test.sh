#!/bin/bash

# This scripts does a simple comparison of two files
# using diff cmd

# Variable to check for errors
error=0

echo "Run code PI"
gcc  pi_con_y_sin_threads.c -o pi -l pthread -lm #> actual_result1.txt
./pi
diff expected_result1.txt actual_result.txt > diff.txt
echo "Show differences "
cat diff.txt
a=`wc -l diff.txt| cut -f1 -d ' '`
if (($a == 0)); then
   echo "Results are correct"
else
  echo "Results are incorrect"
   error=1
fi
echo "Run code MATRIX"
gcc  pi_con_y_sin_threads.c -o pi -l pthread -lm #> actual_result2.txt
./pi
diff expected_result2.txt actual_result2.txt > diff.txt
echo "Show differences "
cat diff.txt
a=`wc -l diff.txt| cut -f1 -d ' '`
if (($a == 0)); then
   echo "Results are correct"
else
  echo "Results are incorrect"
   error=1
fi
rm diff.txt
rm actual_result.txt
rm actual_result2.txt

if(($error == 1));then
	exit -1
fi
#echo "NO se puede hacer el test de la multiplicacion de matrices, debido a que las matrices toman valores n random, tanto cuando se usa paralelismo como cuando no por lo que siempre van a ser diferentes entre ellas y la probabilidad de que el test pase es muy pequeña debido a esto."


